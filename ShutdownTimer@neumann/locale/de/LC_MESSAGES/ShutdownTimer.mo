��    	      d      �       �       �   ]        `     i     x     �     �     �  �  �      �  �   �     V     d     q     �     �     �                  	                            Maximum timer value (in minutes) Root mode (uses 'pkexec shutdown' command,
no interruption of timer, but needs root password) Settings Shutdown Timer Shutdown Timer stopped System will shutdown in min till shutdown minutes Project-Id-Version: Shutdown Timer GNOME Shell Extension
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-05-27 00:46+0100
PO-Revision-Date: 2015-05-27 00:46+0100
Last-Translator: Daniel Neumann
Language-Team: GERMAN Jonatan Zeidler <jonatan_zeidler@gmx.de>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.7.5
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: _
X-Poedit-Basepath: ../../../
X-Poedit-SearchPath-0: .
 Maximaler Uhrenwert (in Minuten) Root Modus (verwendet den 'pkexec shutdown' Befehl,
keine Unterbrechung der Ausschaltuhr möglich, aber benötigt das Root-Passwort) Einstellungen Ausschaltuhr Ausschaltuhr gestoppt System wird herunterfahren in min bis zum Herunterfahren Minuten 